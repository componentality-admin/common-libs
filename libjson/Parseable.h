/****************************************************************************************************/
/* CST Middleware 2009                        * Parseable.h										    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2012-2013                                                       */
/* Copyright (C) Konstantin Khait, 2014                                                             */
/****************************************************************************************************/
/* Abstract description for entity to parse.               											*/
/****************************************************************************************************/

#ifndef PARSEABLE_H
#define PARSEABLE_H

#ifndef ____UNDEFINED
#define ____UNDEFINED (size_t)-1
#endif

#include <string>

namespace CST
{

	namespace Common
	{

		namespace LibJSON
		{

			class Parseable
			{
			public:
				const static std::pair<size_t, size_t> ERROR_VAL;
			protected:
				virtual std::string SkipSpaces(const std::string);
			protected:
				Parseable();
				virtual ~Parseable();
			public:
				virtual std::pair<size_t, size_t> Parse(const std::string&, const size_t, const size_t) = 0;
			}; // class Parseable

		}; // namespace LibJSON

	}; // namespace Common

}; // namespace CST

#endif