/****************************************************************************************************/
/* CST Middleware 2009                        * Serializable.h									    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2012-2013                                                       */
/* Copyright (C) Konstantin Khait, 2014                                                             */
/****************************************************************************************************/
/* Abstract description for object which can be serialized.											*/
/****************************************************************************************************/

#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#ifndef ____UNDEFINED
#define ____UNDEFINED (size_t)-1
#endif


#include <string>
#include <map>

namespace CST
{

	namespace Common
	{

		namespace LibJSON
		{

			typedef std::map<std::string, std::string> OPTIONS;
			class JsonObject;

			class Serializable
			{
			public:
				Serializable();
				virtual ~Serializable();
			public:
				virtual std::string Serialize(const OPTIONS options = OPTIONS()) = 0;
                virtual JsonObject* _Serialize(const OPTIONS options = OPTIONS()) { (void) options; return NULL;};
			};

		}; // namespace LibJSON

	}; // namespace Common

}; // namespace CST

#endif
