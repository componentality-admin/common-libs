/****************************************************************************************************/
/* CST Middleware 2009                        * Serializable.cpp								    */
/****************************************************************************************************/
/* Copyright (C) CST, Konstantin A. Khait, 2008                                                     */
/* Copyright (C) Componentality Oy, 2012-2013                                                       */
/* Copyright (C) Konstantin Khait, 2014                                                             */
/****************************************************************************************************/
/* Abstract description for object which can be serialized.											*/
/****************************************************************************************************/

#include "Serializable.h"

using namespace CST::Common::LibJSON;

Serializable::Serializable()
{
};

Serializable::~Serializable()
{
};
