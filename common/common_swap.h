/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2015                                        */
/*****************************************************************************/
/* Make RAM components swappable to a file system to reduce RAM consumption  */
/*****************************************************************************/
#ifndef COMMON_SWAP_H
#define COMMON_SWAP_H

#include "common_utilities.h"
#include <string>
#include <map>
#include <time.h>

namespace CST
{
	namespace Common
	{
		typedef unsigned long long ssize_t;

		class Swappable;

		class Swap
		{
			friend class Swappable;
		private:
			std::string		mFolder;		// Folder where swapped objects are stored
			mutex			mLock;			// Swap lock
			std::map<std::string, Swappable*> mRegistry; // Registry of swappable objects
			std::map<unsigned long long, Swappable*> mTimes;		 // Time when swappable changed
			unsigned long long mMaxMemory;	// Maximum memory usage
			int	mCounter;					// Unique name counter
			unsigned long long mTotalSize;  // Current total size
		protected:
			virtual std::string makeFileName(); // Create unique file name
			virtual std::string operator+(Swappable&);
			virtual Swap& operator-(Swappable&);
			virtual void onChanged(Swappable&, const unsigned long long old_size, const unsigned long long new_size, const bool noswap = false);
			virtual blob load(const std::string fname);
			virtual void save(const std::string fname, const blob);
			virtual void remove(const std::string fname);
		protected:
			virtual void doSwap(Swappable* exclusion = NULL);
		public:
			Swap(const std::string& folder, const unsigned long long maxmemusage = (unsigned long long) -1);
			virtual ~Swap();
			virtual unsigned long long getLimit() const { return mMaxMemory; }
			virtual void setLimit(const unsigned long long lim);
		};

		Swap& getDefaultSwap();

		class Swappable
		{
			friend class Swap;

		protected:
			enum SwappableStatus
			{
				SWS_ABSENT,
				SWS_CACHED,
				SWS_SWAPPED
			};

			Swap&             mSwap;    // Register of swappable objects
			CST::Common::blob mCache;	// Cached data
			SwappableStatus	  mStatus;	// Current status
			unsigned long long mSize;	// Data size
			unsigned long long mChangeTime; //Time when changed
			std::string		  mName;	// Name in swap
			CST::Common::mutex mLock;
		public:
			Swappable(Swap& swap = getDefaultSwap());
			Swappable(const blob, Swap& swap = getDefaultSwap());
			Swappable(const Swappable&, Swap& swap = getDefaultSwap());
			virtual ~Swappable();
			virtual operator blob();
			virtual Swappable& operator=(const blob);
			virtual Swappable& operator=(const Swappable&);
			virtual unsigned long long getSize() const { return mSize; }
			virtual void swapIn();
			virtual void swapOut(const bool no_notif = false);
			virtual time_t getChangeTime() const { return mChangeTime; }
			virtual void purge();
		};

		class SwapString : protected Swappable
		{
		public:
			SwapString(Swap& swap = getDefaultSwap());
			SwapString(const blob, Swap& swap = getDefaultSwap());
			SwapString(const Swappable&, Swap& swap = getDefaultSwap());
			SwapString(std::string&, Swap& swap = getDefaultSwap());
			virtual ~SwapString();
			virtual operator const std::string();
			virtual SwapString& operator=(const std::string&);
			virtual SwapString& operator=(SwapString&);
			virtual void clear() { purge(); }
			virtual SwapString& operator+=(const std::string val);
			virtual SwapString& operator+=(const char c);
			virtual ssize_t size() { return Swappable::getSize(); }
			virtual char operator[](const size_t index) { return operator const std::string().operator[](index); }
		};

	}
}

#endif
