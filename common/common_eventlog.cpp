#include "common_eventlog.h"
#include <iostream>
#include <time.h>
#include <stdio.h>

#include <memory.h>



#ifndef WIN32
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define closesocket close
#else
#include <Windows.h>
#include <winsock.h>
#define MSG_NOSIGNAL 0x0
typedef int socklen_t;
#endif
using namespace CST::Common;
using namespace CST::Common::LibJSON;

static const char time_key[] = "\"time\"";
static const char source_key[] = "\"source\"";
static const char location_key[] = "\"location\"";
static const char lat_key[] = "\"lat\"";
static const char lon_key[] = "\"lon\"";
static const char severity_key[] = "\"level\"";
static const char msg_key[] = "\"msg\"";
static const char data_key[] = "\"data\"";

void EventLogger::LogServer(void) {
	struct sockaddr_in serv_addr;

	mSock = socket(AF_INET, SOCK_STREAM, 0);

	memset((char *)&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	serv_addr.sin_port = htons(7171);
	serv_addr.sin_family = AF_INET;

	if (connect(mSock, (const sockaddr*)&serv_addr,sizeof(serv_addr)) < 0) {
		closesocket(mSock);
		mSock = -1;
		return;
	}
}

EventLogger& EventLogger::operator()(EventLog& log, std::string data)
{
	OPTIONS options;

	mLocker.lock();


	if (mLogNames.find(&log) != mLogNames.end())
	{
		/* time */
		time_t _time;
		char timestamp[240];

		_time = static_cast<Number*>(log.mEntry[time_key])->GetValue().integer_value;

		tm* utc = gmtime(&_time);
		sprintf(timestamp, "%d/%2.2d/%2.2d %2.2d:%2.2d.%2.2d",
				utc->tm_year + 1900,
				utc->tm_mon + 1,
				utc->tm_mday,
				utc->tm_hour,
				utc->tm_min,
				utc->tm_sec);

		if (mDisplay) {
			this->mLogStream << timestamp << " ";

			if (log.mNoLevel)
				this->mLogStream << "        [" << static_cast<String*>(log.mEntry[source_key])->GetValue() << "]\t";
			else
				this->mLogStream << static_cast<String*>(log.mEntry[severity_key])->GetValue() << " [" <<
					static_cast<String*>(log.mEntry[source_key])->GetValue() << "]\t";

			this->mLogStream << data << std::endl;
			this->mLogStream.flush();
		}
	}

	if (mSock > 0 && !log.mNoLevel) {
		std::string to_send = log.mEntry.Serialize(options);
		if (send(mSock, to_send.c_str(), to_send.size(), MSG_NOSIGNAL) == -1){
			LogServer();
		}
	}

	mLocker.unlock();
	return *this;
}

/***********************************************************************************/
EventLog::EventLog(EventLogger& el, const std::string name) : Log(el, name) {
	
	/* JSON log entry:
	 * time => {zone, timestamp}
	 * location => {lat, lon}
	 * severity =>
	 * message =>
	 * custom properties => {prop, name} ? */
	mNoLevel = true;

	mEntry.AddObject(data_key, new Array());
	mEntry.AddObject(msg_key, new String());

	LibJSON::Object *mLocation = new Object;
	mLocation->AddObject(lat_key, new Number); /* float by default */
	mLocation->AddObject(lon_key, new Number); /* float by default */

	mEntry.AddObject(location_key, mLocation);

	mEntry.AddObject(source_key, new String(name));
	mEntry.AddObject(severity_key, new String());

	mEntry.AddObject(time_key, new Number());
};

EventLog& EventLog::operator<<(enum StartEventLogging severity)
{
	time_t _time;
	std::string err_name;

	mLocker.lock();

	time(&_time);
	*static_cast<Number*>(mEntry[time_key]) = (long int)_time;

	/* severity key */
	mNoLevel = false;
	switch(severity) {
		case EV_INFO   : err_name = std::string("INFO   "); break;
		case EV_WARNING: err_name = std::string("WARNING"); break;
		case EV_ERROR  : err_name = std::string("ERROR  "); break;
		case EV_FAULT  : err_name = std::string("FAULT  "); break;
		default:
			mNoLevel = true;
			break;
	}

	
	static_cast<String*>(mEntry[severity_key])->SetValue(err_name);

	if (static_cast<EventLogger*>(mOwner)->mLocationSet) {
		*static_cast<Number*>(static_cast<Object*>(mEntry[location_key])
				->operator[](lon_key)) = static_cast<EventLogger*>(mOwner)->mLon;

		*static_cast<Number*>(static_cast<Object*>(mEntry[location_key])
				->operator[](lat_key)) = static_cast<EventLogger*>(mOwner)->mLat;
	}

	return *this;
}

EventLog& EventLog::operator<<(EndEventLogging)
{

	static_cast<String*>(mEntry[msg_key])->SetValue(mBuffer);
	static_cast<EventLogger*>(mOwner)->operator()(*this, mBuffer);

	static_cast<Array*>(mEntry[data_key])->Reset();

	mBuffer.clear();
	mLocker.unlock();
	return *this;
}

Log& EventLog::operator<<(StartLogging dummyclass) {
	return Log::operator<<(dummyclass);
}

EventLog& EventLog::operator<<(const std::string data)
{
	mLocker.lock();
	Log::operator<<(data);
	

	mLocker.unlock();
	return *this;
}
	
EventLog& EventLog::operator<<(const int val)
{
	char buf[24];
	sprintf(buf, "%d", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const short int val)
{
	char buf[24];
	sprintf(buf, "%hd", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const long int val)
{
	char buf[24];
	sprintf(buf, "%ld", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const long long int val)
{
	char buf[24];
	sprintf(buf, "%lld", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const size_t val) {
	return operator<<((const long long int)val);
}

EventLog& EventLog::operator<<(const bool val) {
	char buf[24];
	sprintf(buf, "%s", val ? "on(1)" : "off(0)");
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const float val)
{
	char buf[24];
	sprintf(buf, "%f", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const double val)
{
	char buf[24];
	sprintf(buf, "%lf", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const JsonObject *val)
{
	mLocker.lock();
	static_cast<Array*>(mEntry[data_key])->Push((JsonObject*)(val));
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const char val)
{
	char buf[24];
	sprintf(buf, "%c", val);
	mLocker.lock();
	mBuffer += buf;
	mLocker.unlock();

	return *this;
}

EventLog& EventLog::operator<<(const char* str)
{
	return operator<<(std::string(str));
}

EventLog& EventLog::operator<<(const CST::Common::blob blob)
{
    char buf[24];
	mLocker.lock();
    for (size_t i = 0; i < blob.mSize; i++)
    {
		sprintf(buf, "%d", (int) (unsigned char) blob.mData[i]);
		mBuffer += buf;
        mBuffer += ' ';
	}
	mLocker.unlock();
	return *this;
}

void EventLog::SetLocation(float lat, float lon) {
	mLocker.lock();
	static_cast<EventLogger*>(mOwner)->mLocationSet = true;
	static_cast<EventLogger*>(mOwner)->mLat = lat;
	static_cast<EventLogger*>(mOwner)->mLon = lon;
	mLocker.unlock();
}

enum EventLog::StartEventLogging EventLog::EV_BEGIN;
EndEventLogging EventLog::EV_END;
