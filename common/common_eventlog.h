#ifndef COMMON_EVENTLOG_H
#define COMMON_EVENTLOG_H

#include "common_log.h"
#include "common_utilities.h"
#include "../libjson/Json.h"
#include <map>

#ifndef WIN32
#include <unistd.h>
#define closesocket close
#else
#include <Winsock2.h>
#endif

namespace CST
{
	namespace Common
	{
		class EndEventLogging
		{
			bool mDummy;
		};

		class EventLog;

		class EventLogger: public Logger
		{
			friend class EventLog;
		protected:
			bool mLocationSet;
			float mLat, mLon;
			int mSock;
			void LogServer(void);
			bool mDisplay;
		public:
			EventLogger():Logger(), mLocationSet(false) { LogServer(); };
			EventLogger(std::ostream& os): Logger(os), mLocationSet(false) { LogServer(); };
			EventLogger(const std::string str): Logger(str), mLocationSet(false) {LogServer(); };
			EventLogger(const char* c_str): Logger(c_str), mLocationSet(false) {LogServer(); };
			virtual ~EventLogger() { closesocket(mSock); };
			virtual void Enable(void) {mDisplay = true;};
			virtual void Disable(void) {mDisplay = false;};
		protected:
			virtual EventLogger& operator()(EventLog&, std::string data);
		};

		class EventLog: public Log
		{
			friend class EventLogger;
		public:
			enum StartEventLogging
			{
				EV_NONE = 0,
				EV_INFO,
				EV_WARNING,
				EV_ERROR,
				EV_FAULT,
			};

			static enum StartEventLogging EV_BEGIN;
			static      EndEventLogging   EV_END;
			LibJSON::Object mEntry;
		protected:
			bool mNoLevel;
		public:
			EventLog(EventLogger& el, const std::string name);
			virtual ~EventLog() {
				mEntry.Reset();
			};
		public:
			virtual EventLog& operator<<(const std::string);
			virtual EventLog& operator<<(const int);
			virtual EventLog& operator<<(const short int);
			virtual EventLog& operator<<(const long int);
			virtual EventLog& operator<<(const long long int);
			virtual EventLog& operator<<(const size_t);
			virtual EventLog& operator<<(const bool);
			virtual EventLog& operator<<(const float);
			virtual EventLog& operator<<(const double);
			virtual EventLog& operator<<(const LibJSON::JsonObject*);
			virtual EventLog& operator<<(const char);
			virtual EventLog& operator<<(const char*);
			virtual EventLog& operator<<(enum StartEventLogging);
			virtual EventLog& operator<<(EndEventLogging);
			virtual EventLog& operator<<(const CST::Common::blob);
			virtual Log& operator<<(StartLogging);
			void SetLocation(float, float);
		};

	}
}

#endif
