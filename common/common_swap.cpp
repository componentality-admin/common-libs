/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2015                                        */
/*****************************************************************************/
/* Make RAM components swappable to a file system to reduce RAM consumption  */
/*****************************************************************************/
#include "common_swap.h"
#include "../sha1/CompoSHA.h"
#include <stdio.h>

#ifndef WIN32
#include <string.h>
#define STDSWAPFOLDER "/cache"
#else
#include <io.h>
#define STDSWAPFOLDER "c:\\CompoSwap"
#endif

using namespace CST::Common;

std::string Swap::makeFileName()
{
	time_t current_time;
	time(&current_time);
	char result[36];

	mLock.lock();
	sprintf(result, "%u.%d.swap", (unsigned int)current_time, mCounter++);
	mLock.unlock();
	return result;
}

std::string Swap::operator+(Swappable& obj)
{
	mLock.lock();

	std::string name = makeFileName();
	mRegistry[name] = &obj;
	time_t current_time;
	time(&current_time);
	obj.mChangeTime = ((unsigned long long) current_time << 32) + (unsigned long)&obj;
	mTimes[obj.mChangeTime] = &obj;
	mTotalSize += obj.getSize();

	mLock.unlock();
	return name;
}

Swap& Swap::operator-(Swappable& obj)
{
	mLock.lock();

	if (mRegistry.find(obj.mName) != mRegistry.end())
		mRegistry.erase(mRegistry.find(obj.mName));

	if (mTimes.find(obj.mChangeTime) != mTimes.end())
		mTimes.erase(mTimes.find(obj.mChangeTime));

	mTotalSize -= obj.getSize();

	mLock.unlock();
	return *this;
}

void Swap::onChanged(Swappable& obj, const unsigned long long old_size, const unsigned long long new_size, const bool noswap)
{
	mLock.lock();

	mTotalSize = mTotalSize - old_size + new_size;

	if (!noswap)
	{
		if (mTimes.find(obj.mChangeTime) != mTimes.end())
			mTimes.erase(obj.mChangeTime);

		time_t current_time;
		time(&current_time);

		obj.mChangeTime = ((unsigned long long) current_time << 32) + (unsigned long)&obj;
		mTimes[obj.mChangeTime] = &obj;
	}

	mLock.unlock();

	if (!noswap && (mTotalSize > mMaxMemory))
		doSwap(&obj);
}

blob Swap::load(const std::string fname)
{
	return fileRead(fileJoinPaths(mFolder, fname));
}

void Swap::save(const std::string fname, const blob data)
{
	fileWrite(fileJoinPaths(mFolder, fname), data);
}

void Swap::remove(const std::string fname)
{
	::remove(fname.c_str());
}

Swap::Swap(const std::string& folder, const unsigned long long maxmemusage)
{
	mFolder = folder;
	mMaxMemory = maxmemusage;
	mCounter = 0;
	mTotalSize = 0L;
	makeDirectory(folder);
	std::list<std::string> files = listFiles(folder);
	for (std::list<std::string>::iterator i = files.begin(); i != files.end(); i++)
		remove(fileJoinPaths(mFolder, *i));
}

Swap::~Swap()
{
	mLock.lock();
	for (std::map<std::string, Swappable*>::iterator i = mRegistry.begin(); i != mRegistry.end(); i++)
	{
		this->operator-(*i->second);
	}
	mLock.unlock();
}

void Swap::doSwap(Swappable* exclusion)
{
	mLock.lock();
	for (std::map<unsigned long long, Swappable*>::iterator i = mTimes.begin(); i != mTimes.end(); i++)
	{
		if (i->second == exclusion)
			continue;
		if (mTotalSize < mMaxMemory / 2)
			break;
		i->second->swapOut(true);
	}
	mLock.unlock();
}

void Swap::setLimit(const unsigned long long lim)
{
	mLock.lock();
	mMaxMemory = lim;
	mLock.unlock();
	doSwap();
}

///////////////////////////////////////////////////////////////////////////////////////////////////

Swappable::Swappable(Swap& swap) : mSwap(swap)
{
	mCache.mData = NULL;
	mCache.mSize = 0;
	mStatus = SWS_ABSENT;
	mSize = 0L;
	mChangeTime = 0;
	mName = mSwap.operator+(*this);
}

Swappable::Swappable(const blob data, Swap& swap) : mSwap(swap)
{
	mCache.mData = NULL;
	mCache.mSize = 0;
	mStatus = SWS_ABSENT;
	mSize = 0L;
	mChangeTime = 0;
	mName = mSwap.operator+(*this);
	operator=(data);
}

Swappable::Swappable(const Swappable& obj, Swap& swap) : mSwap(swap)
{
	mCache.mData = NULL;
	mCache.mSize = 0;
	mStatus = SWS_ABSENT;
	mSize = 0L;
	mChangeTime = 0;
	mName = mSwap.operator+(*this);
	operator=(obj);
}

Swappable::~Swappable()
{
	purge();
	mSwap.operator-(*this);
}

Swappable::operator blob()
{
	if (mStatus == SWS_SWAPPED)
	{
		swapIn();
	}
	mLock.lock();
	blob result = mCache;
	if (result.mSize)
	{
		result.mData = new char[result.mSize];
		memcpy(result.mData, mCache.mData, result.mSize);
	}
	mLock.unlock();
	return result;
}

Swappable& Swappable::operator=(const blob data)
{
	unsigned long long oldsize = mSize;
	if (mStatus == SWS_SWAPPED)
		swapIn();
	mLock.lock();
	mCache.purge();
	mCache.mSize = data.mSize;
	mCache.mData = new char[mCache.mSize];
	memcpy(mCache.mData, data.mData, mCache.mSize);
	mSize = mCache.mSize;
	mLock.unlock();
	mStatus = SWS_CACHED;
	mSwap.onChanged(*this, oldsize, mSize);
	return *this;
}

Swappable& Swappable::operator=(const Swappable& src)
{
	blob _src = ((Swappable&)src).operator blob();
	operator=(_src);
	_src.purge();
	return *this;
}

void Swappable::swapIn()
{
	if (mStatus == SWS_SWAPPED)
	{
		mLock.lock();
		mCache = mSwap.load(mName);
		mSize = mCache.mSize;
		mStatus = SWS_CACHED;
		mLock.unlock();
		mSwap.onChanged(*this, 0, mSize);
	}
}

void Swappable::swapOut(const bool no_notif)
{
	if (mStatus == SWS_CACHED)
	{
		mLock.lock();
		mSwap.save(mName, mCache);
		mCache.purge();
		mStatus = SWS_SWAPPED;
		mLock.unlock();
		mSwap.onChanged(*this, mSize, 0, no_notif);
	}
}

void Swappable::purge()
{
	mSwap.remove(mName);
	mLock.lock();
	mCache.purge();
	mLock.unlock();
	mSwap.onChanged(*this, (mStatus == SWS_SWAPPED) ? 0 : mSize, 0);
	mSize = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////

static Swap* std_swap = NULL;

Swap& CST::Common::getDefaultSwap()
{
	if (!std_swap)
		std_swap = new Swap(STDSWAPFOLDER,(unsigned long long)-1);
	return *std_swap;
}

/////////////////////////////////////////////////////////////////////////////////////////////

SwapString::SwapString(Swap& swap) : Swappable(swap)
{
}

SwapString::SwapString(const blob data, Swap& swap) : Swappable(data, swap)
{
}

SwapString::SwapString(const Swappable& src, Swap& swap) : Swappable(src, swap)
{
}

SwapString::SwapString(std::string& str, Swap& swap) : Swappable(stringToBlob(str), swap)
{
}

SwapString::~SwapString()
{
	purge();
}

SwapString::operator const std::string()
{
	blob temp = operator blob();
	std::string result = blobToString(temp);
	temp.purge();
	return result;
}

SwapString& SwapString::operator=(const std::string& str)
{
	Swappable::operator=(stringToBlob(str));
	return *this;
}

SwapString& SwapString::operator=(SwapString& src)
{
	Swappable::operator=(stringToBlob(src.operator const std::string()));
	return *this;
}

SwapString& SwapString::operator+=(const std::string val)
{
	std::string result = operator const std::string();
	result += val;
	operator=(result);
	return *this;
}

SwapString& SwapString::operator+=(const char c)
{
	std::string result = operator const std::string();
	result += c;
	operator=(result);
	return *this;
}
