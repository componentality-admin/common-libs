#ifndef ___CRC32_H__
#define ___CRC32_H__

unsigned long crc32(unsigned long crc, const void *buf, size_t size); 

#endif