#ifndef COMMON_LOG_H
#define COMMON_LOG_H

#include "common_utilities.h"
#include <map>

namespace CST
{
	namespace Common
	{
		class StartLogging
		{
			bool mDummy;
		};
		class EndLogging
		{
			bool mDummy;
		};

		class Log;

		class Logger
		{
			friend class Log;
		protected:
			std::map<Log*, std::string> mLogNames;
			mutex mLocker;
			std::ostream& mLogStream;
		public:
			Logger();
			Logger(std::ostream&);
			Logger(const std::string);
			Logger(const char*);
			virtual ~Logger();
		protected:
			virtual Logger& operator()(Log&, std::string data);
			virtual void AddLog(const std::string logname, Log&);
			virtual void RemoveLog(Log&);
		};

		class Log
		{
		public:
			static StartLogging LOG_BEGIN;
			static EndLogging LOG_END;
		protected:
			mutex mLocker;
			Logger* mOwner;
			std::string mBuffer;
		public:
			Log(Logger&, const std::string);
			virtual ~Log();
		public:
			virtual Log& operator<<(const std::string);
			virtual Log& operator<<(const int);
			virtual Log& operator<<(const float);
			virtual Log& operator<<(const char);
			virtual Log& operator<<(const char*);
			virtual Log& operator<<(StartLogging);
			virtual Log& operator<<(EndLogging);
			virtual Log& operator<<(const CST::Common::blob);
		};

	}
}

#endif