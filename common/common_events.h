/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2015                                        */
/*****************************************************************************/
/* Events handling primitives                                                */
/*****************************************************************************/
#ifndef __COMMON_EVENTS_H__
#define __COMMON_EVENTS_H__

#include "common_utilities.h"
#include <list>

namespace CST
{
	namespace Common
	{
		class Event
		{
		public:
			class Payload
			{
			public:
				Payload() {};
				virtual ~Payload() {};
			};
		protected:
			std::string mType;
			Payload* mPayload;
			bool mProcessed;
		public:
			Event() : mPayload(NULL), mProcessed(false) {};
			Event(const std::string type) : mType(type), mPayload(NULL), mProcessed(false) {};
			virtual ~Event() { if (mPayload) CST_DELETE(Payload, mPayload); };
		public:
			virtual std::string getType() const { return mType; }
			virtual void setPayload(Payload& payload) { if (mPayload) CST_DELETE(Payload, mPayload); mPayload = &payload; }
			virtual Payload* getPayload() const { return mPayload; }
			virtual void setProcessed() { mProcessed = true; }
			virtual void clearProcessed() { mProcessed = false; }
			virtual bool getProcessed() const { return mProcessed; }
		};


		class Subscriber
		{
			friend class Dispatcher;
		public:
			Subscriber() {};
			virtual ~Subscriber() {}
		protected:
			virtual bool onEvent(Event&) = 0; // Returns true is event is processed and false if caller needs processing it
		};

		class Dispatcher
		{
		protected:
			std::list<Event*> mQueue;
			std::map<std::string, std::map<Subscriber*, Subscriber*> > mSubscribers;
			std::map<Subscriber*, Subscriber*> mUniversalSubscribers;
			mutex mQueueLock, mSubscribersLock;
		public:
			Dispatcher() {};
			virtual ~Dispatcher() {}
			virtual Event* processEvent();
			virtual void sendEvent(Event&);
			virtual void subscribe(const std::string, Subscriber&);
			virtual void unsubscribe(const std::string type, Subscriber& subscriber);
			virtual void subscribe(Subscriber&);
			virtual void unsubscribe(Subscriber& subscriber);
		};

	} // namespace Common
} // namespace CST

#endif