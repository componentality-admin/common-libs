/*****************************************************************************/
/* Common components library for cross-platform software development         */
/* (C) Componentality Oy, 2008 - 2015                                        */
/*****************************************************************************/
/* Events handling primitives                                                */
/*****************************************************************************/
#include "common_events.h"

using namespace CST::Common;

Event* Dispatcher::processEvent()
{
	if (mQueue.empty())
		return NULL;
	mQueueLock.lock();
	Event& event = *mQueue.front();
	mQueue.pop_front();
	mQueueLock.unlock();

	mSubscribersLock.lock();
	std::map<Subscriber*, Subscriber*> subscribers = mSubscribers[event.getType()];
	for (std::map<Subscriber*, Subscriber*>::iterator i = mUniversalSubscribers.begin(); i != mUniversalSubscribers.end(); i++)
		i->first->onEvent(event);
	for (std::map<Subscriber*, Subscriber*>::iterator i = subscribers.begin(); i != subscribers.end(); i++)
		i->first->onEvent(event);
	mSubscribersLock.unlock();
	return &event;
}

void Dispatcher::sendEvent(Event& event)
{
	mQueueLock.lock();
	mQueue.push_back(&event);
	mQueueLock.unlock();
}

void Dispatcher::subscribe(const std::string type, Subscriber& subscriber)
{
	mSubscribersLock.lock();
	mSubscribers[type][&subscriber] = &subscriber;
	mSubscribersLock.unlock();
}

void Dispatcher::subscribe(Subscriber& subscriber)
{
	mSubscribersLock.lock();
	mUniversalSubscribers[&subscriber] = &subscriber;
	mSubscribersLock.unlock();
}

void Dispatcher::unsubscribe(const std::string type, Subscriber& subscriber)
{
	mSubscribersLock.lock();
	std::map<std::string, std::map<Subscriber*, Subscriber*> >::iterator pos = mSubscribers.find(type);
	if (pos != mSubscribers.end())
	{
		if (pos->second.find(&subscriber) != pos->second.end())
			pos->second.erase(pos->second.find(&subscriber));
	}
	mSubscribersLock.unlock();
}

void Dispatcher::unsubscribe(Subscriber& subscriber)
{
	mSubscribersLock.lock();
	if (mUniversalSubscribers.find(&subscriber) != mUniversalSubscribers.end())
		mUniversalSubscribers.erase(mUniversalSubscribers.find(&subscriber));
	mSubscribersLock.unlock();
}

