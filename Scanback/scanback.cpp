/****************************************************************************/
/* Common Utilities Library        *       (C) Componentality Oy, 2014-2015 */
/****************************************************************************/
/* Scanback compression implementation                                      */
/****************************************************************************/
/* Scanback - LZ77 for embedded systems                                     */
/* Designed and developed by Konstantin Khait                               */
/****************************************************************************/

#include "scanback.h"
extern "C"
{
#include "bitmem.h"
}

#define STORE_NON_REPEATING_SEQUENCE													\
			{																			\
				bitmem_put1(mem, bitcount++, 0);										\
				bitmem_put(mem, bitcount, seq_size - 1, NON_REPEATING_SIZE_BITS);		\
				bitcount += NON_REPEATING_SIZE_BITS;									\
				for (unsigned long j = 0; j < seq_size; j++)							\
				{																		\
					bitmem_put(mem, bitcount, buf[seq_start + j], 8);					\
					bitcount += 8;														\
				}																		\
				seq_size = 0;															\
				seq_start = 0;															\
			}


using namespace Componentality::Common;

// Compare <buf1> and <buf2> for maximum length of <size> and return length of identical fragment
static unsigned char _sb__match(unsigned char* buf1, unsigned char* buf2, unsigned char size)
{
	unsigned char i;
	for (i = 0; i < size; i++)
	{
		if (buf1[i] != buf2[i])
			break;
	}
	return i;
}

// Find maximum matching sequence in buffer <buf> to sequence starting from <index>
// <winsize> - size of window to be scanned in bytes, <matchlen> - maximum length of matching area in bytes, <bufsize> - size of <buf>
SB_PTR _sb_scanback(unsigned char* buf, unsigned long index, unsigned char winsize, unsigned char matchlen, unsigned long bufsize)
{
	SB_PTR result = { 0, 0 };
	unsigned char i;
	if (winsize > index)
		winsize = (unsigned char)index;
	if (matchlen > winsize)
		matchlen = winsize;
	for (i = 1; i < winsize; i++)
	{
		if (buf[index - i] == buf[index])
		{
			register unsigned char matchsize = (unsigned char)(matchlen < (bufsize - index) ? matchlen : bufsize - index);
			if (matchsize > i)
				matchsize = i;
			register unsigned char len = _sb__match(buf + index, buf + index - i, matchsize);
			if ((len > result.length) && (len > 1))
			{
				result.offset = i;
				result.length = len;
			}
		}
	}
	return result;
}

// Do compression of buffer <buf> of size <size> to bitwise memory <mem>. Returns number of produced bits
unsigned long Componentality::Common::SB_Compress(unsigned char* mem, unsigned char* buf, unsigned long size)
{
	unsigned long bitcount = 0, i;
	SB_PTR cptr;
	unsigned long seq_start = 0;								// Start point of sequence of non-repeating items
	unsigned char seq_size = 0;									// Size of sequence of non-repeating items

	for (i = 0; i < (1 << LENGTH_BITS); i++)
		mem[i] = buf[i];
	bitcount += (1 << LENGTH_BITS) * 8;

	for (i = 1 << LENGTH_BITS; i < size;)
	{
		cptr = _sb_scanback(buf, i, 1 << WINDOW_BITS, 1 << LENGTH_BITS, size);
		if (!cptr.offset)
		{
			seq_size += 1;
			if (!seq_start)
				seq_start = i;
			// If maximum length of non-repeating symbols sequence reached, then store it in the output
			if (seq_size == 1 << (NON_REPEATING_SIZE_BITS))
				STORE_NON_REPEATING_SEQUENCE;
			i += 1;
		}
		else
		{
			if (seq_size)
				STORE_NON_REPEATING_SEQUENCE;

			bitmem_put1(mem, bitcount++, 1);
			bitmem_put(mem, bitcount, cptr.offset - 1, WINDOW_BITS);
			bitcount += WINDOW_BITS;
			bitmem_put(mem, bitcount, cptr.length - 1, LENGTH_BITS);
			bitcount += LENGTH_BITS;
			i += cptr.length;
		}
	}
	if (seq_size)
		STORE_NON_REPEATING_SEQUENCE;
	return bitcount;
}

// Initialize decoder context
void Componentality::Common::SB_InitDecoder(SB_DECODER* decoder, unsigned char* mem)
{
	decoder->bitindex = 0;
	decoder->mem = mem;
	decoder->total_decoded = 0;
	decoder->index = 0;
	decoder->brb = 0;
}

// Initialize decoder with ringbuffer
void SB_InitDecoderRB(SB_DECODER* decoder, void* ringbuffer)
{
	decoder->bitindex = 0;
	decoder->mem = 0;
	decoder->total_decoded = 0;
	decoder->index = 0;
	decoder->brb = ringbuffer;
}

// Unpack next byte from the packed stream
unsigned char Componentality::Common::SB_Fetch(SB_DECODER* decoder)
{
	register unsigned char result;
	register unsigned long numchars;
	if (decoder->index < (1 << LENGTH_BITS))
	{
		if (!decoder->brb)
			result = decoder->decoded_buf[decoder->index % (1 << WINDOW_BITS)] = decoder->mem[decoder->index];
		else
			result = decoder->decoded_buf[decoder->index % (1 << WINDOW_BITS)] = (unsigned char)bitmem_read_ringbuf((BIT_RINGBUF*)decoder->brb, 8);
		decoder->index += 1;
		decoder->bitindex += 8;
		decoder->total_decoded += 1;
		return result;
	}
	if (decoder->index >= decoder->total_decoded)
	{
		register bit isref;

		if (!decoder->brb)
			isref = bitmem_get1(decoder->mem, decoder->bitindex++);
		else
			isref = (unsigned char)bitmem_read_ringbuf((BIT_RINGBUF*)decoder->brb, 1);

		if (!isref)
		{
			if (!decoder->brb)
				numchars = (unsigned long)bitmem_get(decoder->mem, decoder->bitindex, NON_REPEATING_SIZE_BITS);
			else
				numchars = (unsigned char)bitmem_read_ringbuf((BIT_RINGBUF*)decoder->brb, NON_REPEATING_SIZE_BITS);
			numchars += 1;
			decoder->bitindex += NON_REPEATING_SIZE_BITS;
			for (unsigned long i = 0; i < numchars; i++)
			{
				if (!decoder->brb)
					decoder->decoded_buf[decoder->total_decoded % (1 << WINDOW_BITS)] = (unsigned char)bitmem_get(decoder->mem, decoder->bitindex, 8);
				else
					decoder->decoded_buf[decoder->total_decoded % (1 << WINDOW_BITS)] = (unsigned char)bitmem_read_ringbuf((BIT_RINGBUF*)decoder->brb, 8);;
				decoder->bitindex += 8;
				decoder->total_decoded += 1;
			}
		}
		else
		{
			register SB_PTR ptr;
			register unsigned char i;
			if (!decoder->brb)
				ptr.offset = (unsigned char)bitmem_get(decoder->mem, decoder->bitindex, WINDOW_BITS) + 1;
			else
				ptr.offset = (unsigned char) bitmem_read_ringbuf((BIT_RINGBUF*)decoder->brb, WINDOW_BITS) + 1;
			decoder->bitindex += WINDOW_BITS;
			if (!decoder->brb)
				ptr.length = (unsigned char)bitmem_get(decoder->mem, decoder->bitindex, LENGTH_BITS) + 1;
			else
				ptr.length = (unsigned char) bitmem_read_ringbuf((BIT_RINGBUF*)decoder->brb, LENGTH_BITS) + 1;
			decoder->bitindex += LENGTH_BITS;
			for (i = 0; i < ptr.length; i++)
			{
				register unsigned long srcptr = decoder->total_decoded - ptr.offset;
				decoder->decoded_buf[decoder->total_decoded % (1 << WINDOW_BITS)] = decoder->decoded_buf[srcptr % (1 << WINDOW_BITS)];
				decoder->total_decoded += 1;
			}
		}
	}
	result = decoder->decoded_buf[decoder->index % (1 << WINDOW_BITS)];
	decoder->index += 1;
	return result;
}
