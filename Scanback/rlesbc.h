/****************************************************************************/
/* Common Utilities Library        *            (C) Componentality Oy, 2015 */
/****************************************************************************/
/* Combined RLE + Scanback implementation (compression is to be done        */
/* sequentially, decompression is optimized                                 */
/****************************************************************************/
#ifndef __RLESBC_H__
#define __RLESBC_H__

#include "rle.h"
#include "scanback.h"

namespace Componentality
{
	namespace Common
	{
		unsigned char RLESB_Fetch(RLE_DECODE* handler, SB_DECODER* sb_handler, unsigned char* repeating_value);
	} // namespace Common
} // namespace Componentality

#endif