#ifndef XJSON_CONV_H
#define XJSON_CONV_H

#include "xjson_vocabulary.h"
#include "xjson_defs.h"
#include "../libjson/Json.h"

namespace CST
{
	namespace Common
	{
		namespace xJSON
		{
			// Text JSON converter to xJSON
			class JSONtoBIN : protected Vocabulary
			{
			protected:
				size_t mVocLimit;
				size_t mLastSize;
			public:
				// If string size is <= strvoclim, it is stored as a vocabulary index, otherwise embedded to the stream
				JSONtoBIN(const size_t strvoclim = 16) { mVocLimit = strvoclim; mLastSize = 0; };
				virtual ~JSONtoBIN() {};
			public:
				// Serialize JSON DOM (but not vocabulary itself)
				virtual std::string operator()(CST::Common::LibJSON::Object&);
				// Serialize JSON string (but not vocabulary itself)
				virtual std::string operator()(const std::string&, size_t& begin, size_t& end);
				// Serialize vocabulary only
				virtual std::string serialize();
				// Do vocabulary sort and provide re-indexing update
				virtual std::string update();
				// Cleanup the vocabulary
				virtual void renew();
			protected:
				// Serialize JSON number
				virtual std::string convert(CST::Common::LibJSON::Number&);
				// Seralize JSON string
				virtual std::string convert(CST::Common::LibJSON::String&, const size_t = 16);
				// Serialize JSON array
				virtual std::string convert(CST::Common::LibJSON::Array&);
				// Serialize JSON object
				virtual std::string convert(CST::Common::LibJSON::Object&);
			};

			// Binary to text JSON conversion
			class BINtoJSON : protected Vocabulary
			{
			public:
				BINtoJSON() {};
				virtual ~BINtoJSON() {};
			public:
				// Convert xJSON to JSON object DOM
				virtual CST::Common::LibJSON::Object* convert(const std::string&);
				// Convert xJSON to string JSON
				virtual std::string operator()(const std::string&);
			protected:
				virtual std::pair<size_t, CST::Common::LibJSON::Object*> readObject(const std::string&, const size_t begin);
				virtual std::pair<size_t, CST::Common::LibJSON::String*> readString(const std::string&, const size_t begin);
				virtual std::pair<size_t, CST::Common::LibJSON::Array*> readArray(const std::string&, const size_t begin);
				CST::Common::LibJSON::JsonObject* getJobj(const std::string& str, size_t& idx);
			};
		}
	}
}

#endif