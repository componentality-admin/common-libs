/***************************************************************************************/
/* Componentailty open software: xJSON                * (C) Componentality Oy, 2013    */
/***************************************************************************************/

#ifndef XJSON_DEFS_H
#define XJSON_DEFS_H

namespace CST
{
	namespace Common
	{
		namespace xJSON
		{
			/* Codes for xJSON objects following this code */
			/* Only 4 of 8 bits are used now, so it can be */
			/* easily compressed with LZ77 or used for     */
			/* extensions                                  */
			enum ITEMTYPE
			{
				NONE			= 0x00,		/* Nothing (should never happen) */
				OBJECT			= 0x01,     /* JSON Object                   */
				ARRAY			= 0x02,     /* JSON Array                    */
				VOCABULARY		= 0x03,     /* Serialized vocabulary         */
				VOCITEM			= 0x04,     /* ID of vocabulary item         */
				POSITIVE_INT	= 0x05,		/* Positive integer (no sign)	 */
				NEGATIVE_INT	= 0x06,		/* Negative integer (no sign)    */
				POSITIVE_FLOAT	= 0x07,		/* Positive float (int + dec)    */
				NEGATIVE_FLOAT	= 0x08,     /* Negative float (int + dec)    */
				BOOL_FALSE		= 0x09,     /* False                         */
				BOOL_TRUE		= 0x0A,     /* True                          */
				NULLVAL			= 0x0B,		/* null                          */
				STRING			= 0x0C,		/* Out of vocabulary ASCIIZ      */
				VOCUPDATE		= 0x0D		/* Vocabulary re-indexing        */
			};
		}
	}
}

#endif